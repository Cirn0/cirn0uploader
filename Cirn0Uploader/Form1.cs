﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cirn0Uploader
{
    public partial class Form1 : Form, IDisposable
    {
        /// <summary>
        /// The enumeration of possible modifiers.
        /// </summary>
        [Flags]
        public enum ModifierKeysEnum : uint
        {
            Alt = 1,
            Control = 2,
            Shift = 4,
            Win = 8
        }

        const int HOTKEY_EVENT_REGISTER_ID = 0x999;

        public const int WM_HOTKEY = 0x0312;

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool RegisterHotKey(IntPtr hWnd, int id, uint fsModifiers, uint vlc);

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool UnregisterHotKey(IntPtr hWnd, int id);

        public Form1()
        {
            InitializeComponent();
            this.ShowInTaskbar = false;
            var res = RegisterHotKey(this.Handle, HOTKEY_EVENT_REGISTER_ID, (uint)(ModifierKeysEnum.Alt | ModifierKeysEnum.Control), (uint)Keys.D5);
            this.FormClosing += Form1_FormClosing;
        }

        protected override void SetVisibleCore(bool value)
        {
            base.SetVisibleCore(false);
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Unregister();
        }

        ~Form1()
        {
            Unregister();
        }

        void Unregister()
        {
            UnregisterHotKey(this.Handle, HOTKEY_EVENT_REGISTER_ID);
        }

        protected override void WndProc(ref Message m)
        {
            base.WndProc(ref m);

            if (m.Msg == WM_HOTKEY && m.HWnd == this.Handle && (int)m.WParam == HOTKEY_EVENT_REGISTER_ID)
            {
                if (Clipboard.ContainsImage())
                {
                    Image img = Clipboard.GetImage();
                    string fileName = Path.GetTempFileName() + ".jpg";
                    img.Save(fileName, System.Drawing.Imaging.ImageFormat.Jpeg);
                    string url = UploadData(fileName);

                    AfterUpload(url);

                    return;
                }
                else if (Clipboard.ContainsFileDropList())
                {
                    var files = Clipboard.GetFileDropList();
                    string url = UploadData(files[0]);

                    AfterUpload(url);

                    return;
                }
                else
                {
                    tray.ShowBalloonTip(2500, "Fail", "Content to upload not found", ToolTipIcon.Error);
                }
            }
        }

        void AfterUpload(string url)
        {
            Clipboard.SetText(url);
            tray.ShowBalloonTip(2500, "Uploaded", url, ToolTipIcon.Info);
        }

        protected string UploadData(string filePath)
        {
            string url = "https://cirn0.info/api/upload";
            using (var webClient = new WebClient())
            {
                var ret = webClient.UploadFile(url, filePath);
                return "https://cirn0.info/" + Encoding.Default.GetString(ret);
            }
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
